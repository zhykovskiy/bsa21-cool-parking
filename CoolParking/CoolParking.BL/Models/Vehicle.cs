﻿using System;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string result = GetLetters() + "-" + GetNumbers() + "-" + GetLetters();
            return result;
        }

        private static string GetNumbers()
        {
            Random rand = new Random();
            return rand.Next(1000, 9999).ToString();
        }

        private static string GetLetters()
        {
            Random rand = new Random();
            return ((char)rand.Next('A', 'Z' + 1) + (char)rand.Next('A', 'Z' + 1)).ToString();
        }
    }
}